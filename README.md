# Configs
Things you might need the lua.rc is still begin worked on but i will update it to time to time here are some things unixporn users use and here are some rc files you can edit and some cheat sheets will be adding more things 
 
color scripts https://gitlab.com/dwt1/shell-color-scripts

cava https://github.com/karlstav/cava

terminal clock https://github.com/xorg62/tty-clock

compton https://github.com/chjj/comp

cool themes for awesome https://github.com/lcpz/awesome-copycatston

https://pwmt.org/projects/zathura/
zathura a doc viewer if you want zathura transparent then you gotta patch the code in the zathura.rc

patch 
https://www.reddit.com/r/unixporn/comments/eq1714/i3_zathura_with_proper_transparency/

https://github.com/b4dtR1p/awesome/blob/master/.xinitrc
this code right here allows you to go into tty and boot up awesome while in your current de or wm 
make sure to paste this code in your .xintrc or check tty.txt in the awesome folder

how to acess tty 
ctrl alt f1 


vim cheat sheet :)
https://vim.rtorr.com

another cheat sheet 
https://github.com/chubin/cheat.sh 
or type cheat.sh in your browser 

cheat.sh
Has a simple curl/browser/editor interface.
Covers 56 programming languages, several DBMSes, and more than 1000 most important UNIX/Linux commands.
Provides access to the best community driven cheat sheets repositories in the world, on par with StackOverflow.
Available everywhere, no installation needed, but can be installed for offline usage.



how to have images in neofetch 
https://github.com/dylanaraps/neofetch/wiki/Images-in-the-terminal


https://surf.suckless.org

surf is a simple web browser based on WebKit2/GTK+. It is able to display websites and follow links. It supports the XEmbed protocol which makes it possible to embed it in another application. Furthermore, one can point surf to another URI by setting its XProperties.

in via terminal 

Start the browser with

surf http://your-url


for my terminal i use alacritty you prob got to install alacritty on your system just so it will work with my dot files https://www.geeksforgeeks.org/how-to-install-and-use-alacritty-terminal-emulator-in-linux/ or this site https://github.com/alacritty/alacritty/blob/master/INSTALL.md
